package sql;

import java.sql.*;
import javax.swing.*;

public class SQLConn{

	public static Connection conn;
	
	private static void connectToDB(){
		try {
			Class.forName("org.sqlite.JDBC");
//			File cwd = new File(".");
			conn = DriverManager.getConnection("jdbc:sqlite:"+System.getProperty("user.dir")+"\\chat.sqlite");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
		
	}
	
	public static void saveToDB(String name, String message){
		try{
			connectToDB();
			String updateQuery = "insert into server_client_chat (name, message) values (?, ?)";
//			Statement stat = conn.createStatement();
			
			PreparedStatement pstmt = conn.prepareStatement(updateQuery);
			pstmt.setString(1, name.substring(0, name.length() - 2));
			pstmt.setString(2, message);
			pstmt.execute();
//			stat.executeUpdate(updateQuery);
//			conn.setAutoCommit(true);
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	public static String getFromDB(){
		String name;
		String message;
		String fullRow = "";
		try{
			connectToDB();
			
			
			Statement stat = conn.createStatement();
			
			ResultSet rs = stat.executeQuery("select * from server_client_chat;");
	        while (rs.next()) {
	            name = rs.getString("name")+": ";
	            message = rs.getString("message");
	            fullRow += name+message+"\n";
	        }
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, e);
		}
		return fullRow;
	}
}
