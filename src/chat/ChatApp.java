package chat;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import sql.*;


public class ChatApp extends Application {

	private boolean isServer = true;
	private TextArea messages;
	private TextField userInput;
	private Networking connection = isServer ? createServer() : createClient();
	private Button clearMsgsWindow;
	
	private Parent createContent(){
		messages = new TextArea();
		messages.setPrefHeight(600);
		messages.setEditable(false);
		userInput = new TextField();
		
		clearMsgsWindow = new Button("Clear messages window");
		
		VBox rootNode = new VBox(25, messages, clearMsgsWindow, userInput);
		VBox.setMargin(messages, new Insets(10, 10, 10, 10));
		VBox.setMargin(userInput, new Insets(10, 10, 10, 10));
		rootNode.setPrefSize(650, 600);
		
		userInput.setOnAction(ae -> {
			String message = isServer ? "Server: " : "Client: ";
			String nameForQuery = message;
			message += userInput.getText();
			
			messages.appendText(message + "\n");
			
			try {
				connection.send(message);
				SQLConn.saveToDB(nameForQuery, userInput.getText());
				userInput.clear();
			} catch (Exception e) {
				messages.appendText("Error occured\n"+e);
			}
		});
		
		clearMsgsWindow.setOnAction(ae -> messages.clear());
		
		return rootNode;
	}
	
	@Override
	public void init() throws Exception {
		connection.startConnection();
	}
	
	@Override
	public void start(Stage mainStage) throws Exception {
		mainStage.setTitle("Chat");
		mainStage.setScene(new Scene(createContent()));
		mainStage.show();
		
		String messagess = SQLConn.getFromDB();
		messages.appendText(messagess);
		
		userInput.requestFocus();
	}
	
	@Override
	public void stop() throws Exception {
		connection.endConnection();
	}
	
	private Server createServer(){
		return new Server(55555, data ->{
			Platform.runLater(() -> {
				messages.appendText(data.toString()+"\n");
			});
		});
	}
	
	private Client createClient(){
		return new Client("127.0.0.1", 55555, data -> {
			Platform.runLater(() -> {
				messages.appendText(data.toString()+"\n");
			});
		});
	}

	public static void main(String[] args) {
		launch(args);
	}

}
