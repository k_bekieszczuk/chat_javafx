package chat;

import java.io.*;
import java.net.*;
import java.util.function.Consumer;

public abstract class Networking {

	private ConnectionThread connThread = new ConnectionThread();
	private Consumer<Serializable> onReceiveCallBack;
	
	public Networking(Consumer<Serializable> onReceiveCallBack){
		this.onReceiveCallBack = onReceiveCallBack;
		connThread.setDaemon(true);
	}
	
	public void startConnection() throws Exception{
		connThread.start();
	}
	
	public void send(Serializable data) throws Exception{
		connThread.out.writeObject(data);
	}

	public void endConnection() throws Exception{
		connThread.socket.close();
	}
	
	protected abstract boolean isServer();
	protected abstract String getIP();
	protected abstract int getPort();
	
	private class ConnectionThread extends Thread{
		private Socket socket;
		private ObjectOutputStream out;
		
		public void run(){
			// creates passive socket for server if is server
			try(ServerSocket server = isServer() ? new ServerSocket() : null;)
			{	 
				if(isServer()) server.bind(new InetSocketAddress("127.0.0.1", 55555));
				// creates active socket for user if is not user, else accepts
				Socket socket = isServer() ? (Socket) server.accept() : new Socket(getIP(), getPort());
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				this.socket = socket;
				this.out = out;
				socket.setTcpNoDelay(true);
				
				while(true){
					Serializable data = (Serializable) in.readObject();
					onReceiveCallBack.accept(data);
				}
			}catch(Exception e){
				onReceiveCallBack.accept("Connection closed"+e);
			}
		}
	}
}
